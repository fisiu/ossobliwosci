<?php

namespace App\DataFixtures;

use App\Entity\Podcast;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PodcastFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $data) {
            $podcast = new Podcast();
            $podcast->setTitle($data['title']);
            $podcast->setAired(DateTime::createFromFormat('Y-m-d', $data['aired']));
            $podcast->setUrl($data['url']);

            $manager->persist($podcast);
        }

        $manager->flush();
    }

    private function getData() : array
    {
        return
        [
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-07-19',
                'url' => 'https://static.prsa.pl//static.prsa.pl/819e852d-5111-4192-a917-19e659b7868c.mp3'
            ],
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-07-05',
                'url' => 'https://static.prsa.pl//static.prsa.pl/92fe71e2-d98e-474a-9744-fb4091052a58.mp3'
            ],
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-06-28',
                'url' => 'https://static.prsa.pl//static.prsa.pl/1313e737-a40d-4fea-85e5-853327eda0c9.mp3'
            ],
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-06-21',
                'url' => 'https://static.prsa.pl//static.prsa.pl/f76acf45-70b4-4e0a-8485-372d05156482.mp3'
            ],
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-06-14',
                'url' => 'https://static.prsa.pl//static.prsa.pl/105d63b0-49e8-4105-9acc-16cc85f642c4.mp3'
            ],
            [
                'title' => 'Trójkowy Ossobowy',
                'aired' => '2019-06-07',
                'url' => 'https://static.prsa.pl//static.prsa.pl/2df67b36-c7fc-4f27-9dd4-b3de0a3a0f6b.mp3'
            ],
        ];
    }
}
