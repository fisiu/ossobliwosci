<?php

namespace App\Utils;

use App\Entity\Podcast;

class Xml
{
    /**
     * @* @param Podcast[] $podcasts Podcasts collection
     */
    public static function generate(array $podcasts) : string
    {
        $xml = <<<xml
        <?xml version='1.0' encoding='UTF-8'?>
        <rss version='2.0'>
        <channel>
        <title>Trójkowy Ossobowy - Archiwum</title>
        <description>Archiwum audycji PR3 Trójkowy Ossobowy, kiedyś pod nazwą Ossobliwości Muzycznie</description>
        <link>https://fidano.pl</link>
        <language>pl-pl</language>
        xml;

        foreach ($podcasts as $podcast) {
            $title = self::xmlEscape($podcast->getTitle());
            $aired = $podcast->getAired()->format('Y-m-d');
            $url = self::xmlEscape($podcast->getUrl());
            $xml .= <<<xml
            <item>
            <title>{$title} - {$aired}</title>
            <link>{$url}</link>
            <guid>{$url}</guid>
            <description>Opis</description>
            <enclosure url="{$url}" type="audio/mpeg"/>
            <pubDate>{$aired}</pubDate>
            </item>
            xml;
        }
        $xml .= "</channel></rss>";

        return $xml;
    }

    private static function xmlEscape($string)
    {
        return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
    }
}
