<?php

namespace App\Controller;

use App\Utils\Xml;
use App\Entity\Podcast;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PodcastController extends AbstractController
{
    /**
     * @Route("/rss", name="rss_feed")
     */
    public function rss()
    {
        $latest = $this->getDoctrine()->getRepository(Podcast::class)->latest(10);

        $response = new Response();
        $response->headers->set("Content-Type", "text/xml");
        $response->setContent(Xml::generate($latest));

        return $response;
    }

    /**
     * @Route("/rss-all", name="rss_all_feed")
     */
    public function rssAll()
    {
        $podcasts = $this->getDoctrine()->getRepository(Podcast::class)->findBy([], ['aired' => 'DESC']);

        $response = new Response();
        $response->headers->set("Content-Type", "text/xml");
        $response->setContent(Xml::generate($podcasts));

        return $response;
    }
}
